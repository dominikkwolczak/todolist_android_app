# ToDo-list-app-Android-
This is my first Android project. My goal is to develop a successful ToDo list app, because everyone needs reminders...

About the Project: 
So being a student at University made me think, I will be needing to remember to do a lot of things and writing them down is essential. That is when I thought to myself; "Why don't I create my own to do list app to write down all those tasks"

After creating the traditional "Hello world" app. I decided to take my ambitions futher and create an app. I found just what I needed. CodePath had a tutorial on how to make a to do list, so I followed it. The plan is to futher develop the app adding my own features to it. 

The initial project was created by CodePath as a tutorial - link: https://github.com/codepath/android_guides 
